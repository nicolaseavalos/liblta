# Changelog

Released versions of liblta

## [3.0] - 2024-03-15

### Added
- pyLTA class with all-in-one communication with LTA
- binary file decoder in Python
- full documentation

### Changed
- old LTA object renamed to legacyLTA
- libABCD is now optional

## [2.0] - 2023-12-04

### Added
- voltages and switches shortcuts
- functions to enable/disable 15v/bias switches 
- publish to `lta/done` the name of the image that was just taken
- `init` and `init_from_json` functions to change LTA object parameters
- `name` attribute to LTA object

### Changed
- set_val(name, value) is now set(name, value)
- `load_config` parses differently, `apply=True` by default
- `write_run_file` is now `write_run_log`
- `add_date` option to `set_image_name`
- `doOneImage` is now `take_image`

### Removed
- lots of superfluous consistency checks
- unused or ill-defined functions
