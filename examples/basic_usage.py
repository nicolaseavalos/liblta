from liblta import pyLTA
import os

# initialize the object
lta = pyLTA()

# load the example configuration file
lta.load_config('example.ini')

# set the run name as '$cwd/images/daq-test'
lta.set_run('daq-test', user='testuser', run_dir=os.path.join(os.getcwd(),'images'))

# send a 'erase+epurge'
lta.erase_epurge()

# reset vsub voltage
lta.setv('vsub', 70)

# perform two cleans of the whole ccd (assuming its size is 1024x768)
for i in range(2):
    lta.take_image('clean', NROW=512, NCOL=400, NSAMP=1, NBINCOL=1, NBINROW=1)

# take a skipper image with specific params
lta.take_image(name='skp', NROW=103, NCOL=400, NSAMP=400, NBINROW=5)

# turn off the skipper
lta.shutdown_ccd()
