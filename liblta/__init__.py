""" All-in-one communication with LTA.

"""

from .pylta import pyLTA
from .legacy_lta import legacyLTA
