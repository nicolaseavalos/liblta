.. _mainpage:

########################
``libLTA`` documentation
########################

:Version: |release|
:Date: |today|

Quick start
-----------

This library provides a python API to send commands to the LTA board. First,
import and initialize the high-level object::

   >>> from liblta import pyLTA
   >>> lta = pyLTA()

Next, connect to the LTA assiging a name (in this case, "myLTA"), the host IP,
and the board IP and port::

   >>> lta.connect('myLTA', host_ip='192.168.133.100',
                   board_ip='192.168.133.7', board_port=2001)

Finally, send a command. For example::

   >>> lta.send('get telemetry all')

The :doc:`send <generated/liblta.pyLTA.send>` method allows to send "raw"
messages to the LTA firmware. However, there are several convenience functions
that enable the user to send more useful commands in a pythonic fashion. Check
the :doc:`pyLTA <pylta_api>` page to see the full documentation.

.. toctree::
   :maxdepth: 3
   :caption: Contents

   reference
