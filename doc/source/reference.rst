.. module:: liblta

.. _reference:

################
libLTA reference
################

:Release: |release|
:Date: |today|

This reference page details functions, modules, and objects included in libLTA,
describing what they are and what they do.

.. toctree::
    :maxdepth: 2
    :caption: Contents

    pylta_api
    legacy_api
