.. currentmodule:: liblta

.. _legacy_api:

*******************************
Legacy API (:class:`legacyLTA`)
*******************************

.. autosummary:: legacyLTA
