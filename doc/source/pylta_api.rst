.. currentmodule:: liblta

.. _pylta_api:

*******************************
python-LTA API (:class:`pyLTA`)
*******************************

pyLTA class
===========

The :class:`pyLTA` class provides a set of convenience functions that
makes it easy for the user to operate the LTA.

.. autosummary::
    :toctree: generated/

    pyLTA

In the rest of the page we will assume that one has initialized a ``pyLTA``
object and called it ``lta``::

    >>> lta = pyLTA()

Quick configuration
===================

Connection information, sequencer, and internal variables can be configured
directly from a `.ini` file using the following method:

.. autosummary::
    :toctree: generated/

    pyLTA.load_config

Enabling LTA switches
=====================

Before taking an image, ensure that the 15V and bias switches are closed:

.. autosummary::
    :toctree: generated/

    pyLTA.enable_15v_switches
    pyLTA.enable_bias_switches

Taking an image
===============

We are all set to take images. The directory where the images will be saved can
be set with :meth:`pyLTA.set_run`. The method :meth:`pyLTA.take_image` is a
shortcut where the image name and additional parameters can be changed.

.. autosummary::
    :toctree: generated/

    pyLTA.set_run
    pyLTA.take_image

Erase-epurge
============

This convenience method wraps in a single function both `ccd_erase` and
`ccd_epurge` calls.

.. autosummary::
    :toctree: generated/

    pyLTA.erase_epurge

Shutting down the CCD
=====================

If you want to finish your work day and leave the CCD in a "stand-by" mode, use
the following method:

.. autosummary::
    :toctree: generated/

    pyLTA.shutdown_ccd

Lower level methods
===================

The following methods allow for a better fine-tuning of whatever one wants to
do with the LTA.

Setting LTA internal variables
------------------------------

Firmware variables can be changed or inspected using the following methods:

.. autosummary::
    :toctree: generated/

    pyLTA.setv
    pyLTA.getv

Setting CCD voltages
--------------------

Although the LTA allows to configure each clock voltage in an independent
fashion, common practice is to assign the same value to many of them. In order
to make this easier, one can configure shortcuts. For example, in the default
configuration, instead of the following block of code::

    >>> vh = -1.    # set vertical clocks' high value to -1V
    >>> vl = -4.    # set vertical clocks' high value to -4V
    >>> for vname in ['v1ah', 'v1bh', 'v2ch', 'v3ah', 'v3bh']:
    >>>     lta.setv(vname, vh)
    >>> for vname in ['v1al', 'v1bl', 'v2cl', 'v3al', 'v3bl']:
    >>>     lta.setv(vname, vl)

One can simply do::

    >>> lta.set_voltages({'vh': -1, 'vl': -4})

.. autosummary::
    :toctree: generated/

    pyLTA.set_voltages

Voltage shortcuts are, by default, set in the following way::

    >>> lta.voltage_shortcuts = {
                                'vh': ['v1ah', 'v1bh', 'v2ch', 'v3ah', 'v3bh'],
                                'vl': ['v1al', 'v1bl', 'v2cl', 'v3al', 'v3bl'],
                                'hh': ['h1ah', 'h1bh', 'h2ch', 'h3ah', 'h3bh'],
                                'hl': ['h1al', 'h1bl', 'h2cl', 'h3al', 'h3bl'],
                                'th': ['tgah', 'tgbh'],
                                'tl': ['tgal', 'tgbl'],
                                'sh': ['swah', 'swbh'],
                                'sl': ['swal', 'swbl'],
                                'rh': ['rgah', 'rgbh'],
                                'rl': ['rgal', 'rgbl'],
                                'oh': ['ogah', 'ogbh'],
                                'ol': ['ogal', 'ogbl'],
                                'dh': ['dgah', 'dgbh'],
                                'dl': ['dgal', 'dgbl']
                                }

Similarly, bias and 15v switches are::

    >>> lta.switch_shortcuts = {
                                'pm15v': ['p15v_sw', 'm15v_sw'],
                                'bias': ['vdrain_sw', 'vdd_sw', 'vsub_sw', 'vr_sw']
                                }

Both dictionaries can be changed by updating the corresponding attribute of the
``pyLTA`` object.

Sequencer and readout
---------------------

You can fine-tune what to do with the sequencer, rather than just taking the
whole image.

.. autosummary::
    :toctree: generated/

    pyLTA.set_sequencer
    pyLTA.run_sequencer
    pyLTA.stop_sequencer
    pyLTA.start_readout
    pyLTA.run_readout
    pyLTA.stop_readout

Low-level communication with LTA
--------------------------------

.. autosummary::
    :toctree: generated/

    pyLTA.connect
    pyLTA.send

Other pyLTA methods
-------------------

.. autosummary::
    :toctree: generated/

    pyLTA.apply_config
    pyLTA.set_output_dir
    pyLTA.write_run_log
    pyLTA.set_image_name
    pyLTA.reset_image_id
    pyLTA.disable_15v_switches
    pyLTA.disable_bias_switches
